import * as Actions from 'actions/summaryStatus';
import { formatCovidDetail } from 'helpers/formatData';

export const initialState = {
  type: '',
  isLoading: false,
  error: '',
  detailStatus: [],
};

const covidDetail = (state = initialState, action) => {
  const { errors, type, data } = action;

  switch (type) {
    case Actions.GET_DETAIL_COVID_STATUS:
      return {
        ...state,
        type,
        isLoading: true,
      };

    case Actions.GET_DETAIL_COVID_STATUS_SUCCESS:
      return {
        ...state,
        type,
        isLoading: false,
        detailStatus: formatCovidDetail(data),
        error: initialState.error,
      };

    case Actions.GET_DETAIL_COVID_STATUS_FAILED:
      return {
        ...state,
        type,
        isLoading: false,
        error: errors.message,
      };

    default:
      return state;
  }
};

export default covidDetail;
