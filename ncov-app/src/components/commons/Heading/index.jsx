// Libs
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';

type Props = {
  headingContent: String;
}

const Heading = ({ headingContent }: Props) => (
  <h2 className="heading">{headingContent}</h2>
);

export default memo(Heading, isEqual);
