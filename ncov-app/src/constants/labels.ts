// Define the label name constants
const LABEL_NAMES = {
  SEARCH_BY_COUNTRY: 'Search By Country',
};

export default LABEL_NAMES;
