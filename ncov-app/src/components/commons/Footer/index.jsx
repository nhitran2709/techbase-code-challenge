// Libs
import React, { memo } from 'react';

const MainFooter = () => (
  <footer className="main-footer">
    Created by Nhi Tran
  </footer>
);

export default memo(MainFooter);
