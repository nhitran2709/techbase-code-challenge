// Libs
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

// Constants
import TABLE_HEAD_LIST from 'constants/table';

// Components
import NcovTableBody from '.';

const props = {
  dataTable: TABLE_HEAD_LIST.COV_GENERAL_INFO,
};

describe('NcovTableBody component', () => {
  const wrapper = shallow(<NcovTableBody {...props} />);

  // Test Snapshots
  it('should render correctly UI', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
