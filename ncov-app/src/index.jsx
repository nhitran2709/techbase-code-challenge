// Libs
import React from 'react';
import ReactDOM from 'react-dom';

// Styles
import './index.scss';

// Component
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
);
