// Models
import { NcovStatusType, NcovidDetailType } from 'models';

/**
 * formatCovidSumData: Function format data covid summary
 * @param data: Object
 */
const formatCovidSumData = (data) => {
  const { Global, Countries } = data;
  const covidSumStatus: NcovStatusType[] = [];
  const global = {
    country: 'Global',
    countryCode: 'Global',
    newConfirm: Global.NewConfirmed,
    newDeaths: Global.NewDeaths,
    newRecovered: Global.NewRecovered,
    totalConfirmed: Global.TotalConfirmed,
    totalDeaths: Global.TotalDeaths,
    totalRecovered: Global.TotalRecovered,
    slug: Global.Slug,
  };
  let countryData: NcovStatusType;

  covidSumStatus.push(global);
  Countries.map((country) => {
    countryData = {
      country: country.Country,
      countryCode: country.CountryCode,
      newConfirm: country.NewConfirmed,
      newDeaths: country.NewDeaths,
      newRecovered: country.NewRecovered,
      totalConfirmed: country.TotalConfirmed,
      totalDeaths: country.TotalDeaths,
      totalRecovered: country.TotalRecovered,
      slug: country.Slug,
    };
    return covidSumStatus.push(countryData);
  });
  return covidSumStatus;
};

/**
 * formatCovidDetail: Function helper format covid detailed data by country
 * @param data Object
 */
const formatCovidDetail = (data) => {
  let covidDetail: NcovidDetailType;
  const covidDetailData: NcovidDetailType[] = [];
  data.map((covidCase) => {
    covidDetail = {
      active: covidCase.Active,
      confirmed: covidCase.Confirmed,
      deaths: covidCase.Deaths,
      recovered: covidCase.Recovered,
    };
    return covidDetailData.push(covidDetail);
  });
  return covidDetailData;
};

export { formatCovidSumData, formatCovidDetail };
