// Libs
import { put, call, takeLatest } from 'redux-saga/effects';

import API from 'apis';

// Constants
import * as Actions from 'actions/summaryStatus';

/**
 * Get covid detail status
 */
function* getDetailCovidStatus(actions) {
  const { countryCode } = actions.payload;

  try {
    const response = yield call(() => API.get(`/country/${countryCode}`));

    if (response.ok) {
      yield put({
        type: Actions.GET_DETAIL_COVID_STATUS_SUCCESS,
        data: response.data,
      });
    } else {
      yield put({
        type: Actions.GET_DETAIL_COVID_STATUS_FAILED,
        errors: response.data,
      });
    }
  } catch (errors) {
    yield put({
      type: Actions.GET_DETAIL_COVID_STATUS_FAILED,
      errors,
    });
  }
}

function* covidDetail() {
  return [
    yield takeLatest(Actions.GET_DETAIL_COVID_STATUS, getDetailCovidStatus),
  ];
}

export default covidDetail;
