import * as Actions from 'actions/summaryStatus';
import { formatCovidSumData } from 'helpers/formatData';

export const initialState = {
  type: '',
  isLoading: false,
  error: '',
  sumStatus: [],
};

const covidSumStatus = (state = initialState, action) => {
  const { errors, type, data } = action;

  switch (type) {
    case Actions.GET_SUMMARY_COVID_STATUS:
      return {
        ...state,
        type,
        isLoading: true,
      };

    case Actions.GET_SUMMARY_COVID_STATUS_SUCCESS:
      return {
        ...state,
        type,
        isLoading: false,
        sumStatus: formatCovidSumData(data),
        error: initialState.error,
      };

    case Actions.GET_SUMMARY_COVID_STATUS_FAILED:
      return {
        ...state,
        type,
        isLoading: false,
        error: errors.message,
      };

    default:
      return state;
  }
};

export default covidSumStatus;
