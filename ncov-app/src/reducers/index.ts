// Libs
import { combineReducers } from 'redux';

// Reducers
import covidDetail from 'reducers/covidDetail';
import covidSumStatus from 'reducers/covidSummary';

const appReducers = combineReducers({
  covidDetail,
  covidSumStatus,
});

const rootReducers = (state, action) => appReducers(state, action);

export default rootReducers;
