// Libs
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

// Constants
import PLACEHOLDERS from 'constants/placeholders';
import LABEL_NAMES from 'constants/labels';

// Components
import SearchGroup from '.';

const props = {
  idInput: 'text',
  labelName: LABEL_NAMES.SEARCH_BY_COUNTRY,
  placeholderName: PLACEHOLDERS.SEARCH_BY_COUNTRY,
  valueInput: '',
  inputType: 'text',
  onHandleChange: jest.fn(),
};

describe('SearchGroup component', () => {
  const wrapper = shallow(<SearchGroup {...props} />);

  // Test Snapshots
  it('should render correctly UI', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
