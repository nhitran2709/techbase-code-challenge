// Libs
import React, { memo } from 'react';

const Indicator = () => (
  <div className="indicator">
    <span className="indicator__img" />
  </div>
);

export default memo(Indicator);
