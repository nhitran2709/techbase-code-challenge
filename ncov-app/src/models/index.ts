export interface NcovStatusType {
    country: string,
    countryCode: string,
    slug: string,
    newConfirm: number,
    newDeaths: number,
    newRecovered: number,
    totalConfirmed: number,
    totalDeaths: number,
    totalRecovered: number,
}

export interface NcovidDetailType {
  active: number,
  confirmed: number,
  deaths: number,
  recovered: number,
}
