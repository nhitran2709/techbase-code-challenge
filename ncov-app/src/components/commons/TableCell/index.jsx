// Libs
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';

type Props = {
  cellContent: String;
}

const TableCell = ({ cellContent }: Props) => (
  <div className="table__cell">
    <span>{cellContent}</span>
  </div>
);

export default memo(TableCell, isEqual);
