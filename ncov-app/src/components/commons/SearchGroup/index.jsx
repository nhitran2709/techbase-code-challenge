// Libs
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';

type Props = {
  idInput: String;
  labelName: String;
  inputType: String;
  valueInput: String;
  placeholderName: String;
  onHandleChange: () => void;
  onHandleSubmit: () => void;
}

const SearchGroup = ({
  idInput,
  labelName,
  inputType,
  valueInput,
  placeholderName,
  onHandleChange,
  onHandleSubmit,
}: Props) => (
  <form className="search-group" onSubmit={onHandleSubmit}>
    <label htmlFor={idInput} className="search-group__label">
      {labelName}
      <input
        id={idInput}
        className="search-group__input"
        type={inputType}
        value={valueInput}
        placeholder={placeholderName}
        onChange={onHandleChange}
      />
    </label>
  </form>
);

export default memo(SearchGroup, isEqual);
