// Libs
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

// Components
import Heading from '.';

const props = {
  headingContent: 'heading content',
};

describe('Heading component', () => {
  const wrapper = shallow(<Heading {...props} />);

  // Test Snapshots
  it('should render correctly UI', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
