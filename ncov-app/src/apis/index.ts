import { create } from 'apisauce';

// Create base URL
const API = create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    'X-Access-Token': '5cf9dfd5-3449-485e-b5ae-70a60e997864',
  },
});

export default API;
