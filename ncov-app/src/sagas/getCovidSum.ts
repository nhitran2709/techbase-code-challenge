// Libs
import { put, call, takeLatest } from 'redux-saga/effects';

import API from 'apis';

// Constants
import * as Actions from 'actions/summaryStatus';

/**
 * Get covid summary status
 */
function* getSummaryCovidStatus() {
  try {
    const response = yield call(() => API.get('/summary'));

    if (response.ok) {
      yield put({
        type: Actions.GET_SUMMARY_COVID_STATUS_SUCCESS,
        data: response.data,
      });
    } else {
      yield put({
        type: Actions.GET_SUMMARY_COVID_STATUS_FAILED,
        errors: response.data,
      });
    }
  } catch (errors) {
    yield put({
      type: Actions.GET_SUMMARY_COVID_STATUS_FAILED,
      errors,
    });
  }
}

function* covidSummary() {
  return [
    yield takeLatest(Actions.GET_SUMMARY_COVID_STATUS, getSummaryCovidStatus),
  ];
}

export default covidSummary;
