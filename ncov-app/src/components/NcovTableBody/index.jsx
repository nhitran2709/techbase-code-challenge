// Libs
import React, { memo } from 'react';

// models
import { NcovStatusType } from 'models';

// Components
import TableRow from 'components/commons/TableRow';
import isEqual from 'react-fast-compare';

type Props = {
  dataTable: NcovStatusType[],
}

const NcovTableBody = ({ dataTable }: Props) => (
  <div className="table__body">
    {
      dataTable.map((item) => (
        <TableRow tableRow={item} idRow={item.slug} />
      ))
    }
  </div>
);

export default memo(NcovTableBody, isEqual);
