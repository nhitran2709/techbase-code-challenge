// Libs
import React, { memo, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';

// Components
import NcovTableBody from 'components/NcovTableBody';
import NcovTableHead from 'components/NcovTableHead';
import Heading from 'components/commons/Heading';
import TABLE_HEAD_LIST from 'constants/table';

const DetailScreen = () => {
  const location = useLocation();
  const { covidDetailState } = location.state;
  const [covidDetailData, setCovidDetailData] = useState(covidDetailState);

  useEffect(() => {
    if (covidDetailState) {
      setCovidDetailData(covidDetailState);
    }
  }, [covidDetailState]);

  return (
    <>
      <section className="home-screen">
        <Heading headingContent="situation detail COVID-19 case by a country" />
        <div>
          <NcovTableHead tableHeadData={TABLE_HEAD_LIST.COVID_DETAIL_INFO_HEAD} isTableHead />
          <NcovTableBody dataTable={covidDetailData} />
        </div>
      </section>
    </>
  );
};

export default memo(DetailScreen);
