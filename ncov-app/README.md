# Techbase Vietnam Code Challenge

## Dev

Tran Thi Yen Nhi

## Timeline

Dec 22, 2020 ~ Dec 25, 2020

## Development plan

[The development plan](http://bit.ly/37NrYt6)

## Repo

- Repo: https://gitlab.com/nhitran2709/techbase-code-challenge
- Run: cd `techbase-code-challenge`
- Pull code from master branch to local
- cd `ncov-app`

## Quick start

1.  **Install dependencies.**

    Install dependencies

    ```sh
    yarn install
    ```

2.  **Setup App** Create a .env.local file in root folder Copy and paste the whole content in .env.example.local file into .env.local which was created Change the value after = symbol

3.  **Runs the Research site in the development mode**

    ```sh
    yarn run start
    ```

    Runs the site in the development mode.<br> Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

    The page will reload if you make edits.<br>

4.  **Runs Unit Test**

    ```sh
    yarn test
    ```

    Launches the test runner in the interactive watch mode.<br> See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Directory structure

A quick look at the top-level files and directories you'll see in project.

    .
    ├── node_modules
    ├── src
    │   ├── actions
    │   ├── apis
    │   ├── assets
    │   ├── components
    │   ├── constants
    │   ├── helpers
    │   ├── layouts
    │   ├── models
    │   ├── reducers
    │   ├── sagas
    │   ├── screens
    │   ├── store
    ├── App.jsx
    ├── App.test.js
    ├── index.jsx
    ├── index.scss
    ├── lerna.json
    ├── setupTests.js
    ├── .babelrc
    ├── .editorconfig
    ├── .env
    ├── .env.example
    ├── .eslintrc.js
    ├── .gitignore
    ├── .prettierrc
    ├── jest.config.js
    ├── package.json
    ├── README.md
    ├── tsconfig.json
    └── yarn.lock

1.  **`/node_modules`**: The directory where all of the modules of code that your project depends on (npm packages) are automatically installed.

2.  **`/packages`**: This directory will contain all of the code related to what you will see on the front-end of your site.

3.  **`.gitignore`**: This file tells git which files it should not track / not maintain a version history for.

4.  **`package.json`**: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project.

5.  **`README.md`**: A text file containing useful reference information about your project.

6.  **`yarn.lock`**: [Yarn](https://yarnpkg.com/) is a package manager alternative to npm. You can use either yarn or npm, though all of the Gatsby docs reference npm. This file serves essentially the same purpose as `package-lock.json`, just for a different package management system.

## Versioning

This project using the following versioning:

- Node 12.0.0
- Yarn 1.22.4
