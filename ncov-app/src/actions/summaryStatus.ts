// Define actions to get summary status about COVID-19 by country
export const GET_SUMMARY_COVID_STATUS = 'GET_SUMMARY_COVID_STATUS';
export const GET_SUMMARY_COVID_STATUS_SUCCESS = 'GET_SUMMARY_COVID_STATUS_SUCCESS';
export const GET_SUMMARY_COVID_STATUS_FAILED = 'GET_SUMMARY_COVID_STATUS_FAILED';

// Define actions to get detail status about COVID-19
export const GET_DETAIL_COVID_STATUS = 'GET_DETAIL_COVID_STATUS';
export const GET_DETAIL_COVID_STATUS_SUCCESS = 'GET_DETAIL_COVID_STATUS_SUCCESS';
export const GET_DETAIL_COVID_STATUS_FAILED = 'GET_DETAIL_COVID_STATUS_FAILED';

/**
 * Get summary status of COVID-19 by country
 */
export const getSummaryCovidStatus = () => ({
  type: GET_SUMMARY_COVID_STATUS,
});

/**
 * Get summary status of COVID-19 by country
 */
export const getDetailCovidStatus = (value) => ({
  type: GET_DETAIL_COVID_STATUS,
  payload: value,
});
