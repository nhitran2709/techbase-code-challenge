// Libs
import React from 'react';
import { Provider } from 'react-redux';
import store from 'store';

// Screens
import Screens from 'screens';

const App = () => (
  <div className="app">
    <Provider store={store}>
      <Screens />
    </Provider>
  </div>
);

export default App;
