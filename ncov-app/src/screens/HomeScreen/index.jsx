// Libs
import React, { memo, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Actions
import * as Actions from 'actions/summaryStatus';

// Components
import NcovTableBody from 'components/NcovTableBody';
import NcovTableHead from 'components/NcovTableHead';
import Heading from 'components/commons/Heading';
import SearchGroup from 'components/commons/SearchGroup';
import Indicator from 'components/commons/Indicator';
import TABLE_HEAD_LIST from 'constants/table';

const HomeScreen = () => {
  const dispatch = useDispatch();
  const covidSumStatus = useSelector((state: any) => state.covidSumStatus);
  const { sumStatus, isLoading } = covidSumStatus;
  const [covidCases, setCovidCase] = useState(sumStatus);
  const [searchValue, setSearchValue] = useState('');
  // const [searchResults, setSearchResult] = useState([]);

  useEffect(() => {
    dispatch(Actions.getSummaryCovidStatus());
  }, []);

  useEffect(() => {
    // eslint-disable-next-line no-extra-boolean-cast
    if (sumStatus) {
      setCovidCase(sumStatus);
    }
  }, [sumStatus]);

  const handleSearching = (covidData, searchContent) => {
    const result = covidData.filter(
      (countryInfo) => (
        countryInfo.country.toLowerCase()) === searchContent,
    );
    setCovidCase(result);
  };

  const handleOnChange = (e) => {
    const { value } = e.target;
    const searchContent = value.toLowerCase();

    setSearchValue(searchContent);
  };

  const handleClick = (e) => {
    e.preventDefault();
    handleSearching(covidCases, searchValue);
  };

  return (
    <>
      {isLoading && <Indicator />}
      <section className="search-group-section">
        <SearchGroup
          idInput="searchCountry"
          labelName="search by country: "
          inputType="text"
          valueInput={searchValue}
          placeholderName="search by country"
          onHandleChange={handleOnChange}
          onHandleSubmit={handleClick}
        />
      </section>
      <section className="home-screen">
        <Heading headingContent="situation by country" />
        <div>
          <NcovTableHead tableHeadData={TABLE_HEAD_LIST.COV_GENERAL_INFO_HEAD} isTableHead />
          <NcovTableBody dataTable={covidCases} />
        </div>
      </section>
    </>
  );
};

export default memo(HomeScreen);
