/* eslint-disable jsx-a11y/no-static-element-interactions */
// Libs
import React, { memo, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import isEqual from 'react-fast-compare';

// Actions
import * as Actions from 'actions/summaryStatus';

// Routes
import ROUTES from 'constants/routes';

// Components
import TableCell from '../TableCell';

type Props = {
  tableRow: [
    {
      cellContent: string,
      id: number
    }
  ],
  isTableHead: boolean,
  idRow: string,
}

const TableRow = ({ tableRow, isTableHead, idRow }: Props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const covidDetail = useSelector((state: any) => state.covidDetail);
  // const { detailStatus, isLoading } = covidDetail;
  const [countryCode, setCountryCode] = useState('');
  const { type, isLoading, detailStatus } = covidDetail;
  const [covidDetailData, setCovidDetailData] = useState(detailStatus);

  useEffect(() => {
    if (countryCode) {
      dispatch(Actions.getDetailCovidStatus({ countryCode }));
    }
  }, [countryCode]);

  useEffect(() => {
    if (type === Actions.GET_DETAIL_COVID_STATUS_SUCCESS) {
      setCovidDetailData(detailStatus);
      history.push(ROUTES.DETAIL, {
        covidDetailState: covidDetailData,
        type,
        isLoading,
      });
    }
  }, [detailStatus]);

  const handleClick = (e) => {
    const countryId = e.target.parentNode.id;
    setCountryCode(countryId);
  };

  return (
    // eslint-disable-next-line jsx-a11y/click-events-have-key-events
    <div className="table__row" id={idRow} onClick={handleClick}>
      {
        isTableHead ? tableRow.map((item) => (
          <TableCell cellContent={item} key={item} />
        )) : Object.keys(tableRow).map((key) => (
          <TableCell cellContent={(key !== 'slug' && key !== 'countryCode' && tableRow[key] !== null) && tableRow[key]} key={key} />
        ))
      }
    </div>
  );
};

export default memo(TableRow, isEqual);
