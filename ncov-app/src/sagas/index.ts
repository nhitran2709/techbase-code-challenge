// Libs
import { all } from 'redux-saga/effects';
import covidSummary from 'sagas/getCovidSum';
import covidDetail from 'sagas/getCovidDetail';

export default function* rootSaga() {
  yield all([covidSummary(), covidDetail()]);
}
