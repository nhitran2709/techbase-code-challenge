// Libs
import React, { memo } from 'react';

const MainHeader = () => (
  <header className="main-header">
    <h1>
      <a href="/" className="main-header__logo">
        Corona Information
      </a>
    </h1>
  </header>
);

export default memo(MainHeader);
