// Libs
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

// Components
import TableCell from '.';

const props = {
  cellContent: 'cell content',
};

describe('TableCell component', () => {
  const wrapper = shallow(<TableCell {...props} />);

  // Test Snapshots
  it('should render correctly UI', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
