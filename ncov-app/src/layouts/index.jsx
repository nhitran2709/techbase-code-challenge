// Libs
import React from 'react';

// Components
import MainHeader from 'components/commons/Header';
import MainFooter from 'components/commons/Footer';

type Props = {
  children: React.ReactNode
}

const MainLayout = ({ children }: Props) => (
  <>
    <MainHeader />
    {children}
    <MainFooter />
  </>
);

export default MainLayout;
