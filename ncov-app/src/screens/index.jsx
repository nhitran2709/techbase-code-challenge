import React, { Suspense, lazy } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ROUTES from 'constants/routes';

// Components
import Indicator from 'components/commons/Indicator';
import MainLayout from 'layouts';

// Screens
const HomeScreen = lazy(() => import('./HomeScreen'));
const DetailScreen = lazy(() => import('./DetailScreen/'));

const Screens = () => (
  <MainLayout>
    <Suspense fallback={<Indicator />}>
      <BrowserRouter>
        <Switch>
          <Route exact path={ROUTES.HOME} component={HomeScreen} />
          <Route path={ROUTES.SURVEY} component={DetailScreen} />
        </Switch>
      </BrowserRouter>
    </Suspense>
  </MainLayout>
);

export default Screens;
