// Libs
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

// Constants
import TABLE_HEAD_LIST from 'constants/table';

// Components
import HomeScreen from '.';

const props = {
  tableHeadData: TABLE_HEAD_LIST.COV_GENERAL_INFO_HEAD,
  dataTable: TABLE_HEAD_LIST.COV_GENERAL_INFO,
};

describe('HomeScreen component', () => {
  const wrapper = shallow(<HomeScreen {...props} />);

  // Test Snapshots
  it('should render correctly UI', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
