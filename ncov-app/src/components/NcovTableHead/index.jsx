// Libs
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';

// Components
import TableRow from 'components/commons/TableRow';

type Props = {
  tableHeadData: Array<string>
}

const NcovTableHead = ({ tableHeadData }: Props) => (
  <div className="table__head">
    <TableRow tableRow={tableHeadData} isTableHead />
  </div>
);

export default memo(NcovTableHead, isEqual);
