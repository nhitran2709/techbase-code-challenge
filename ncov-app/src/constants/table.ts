const TABLE_HEAD_LIST = {
  COV_GENERAL_INFO_HEAD: ['name', 'current cases', 'total cases', 'deaths', 'deaths total', 'recovered', 'recovered total'],
  COVID_DETAIL_INFO_HEAD: ['active', 'confirmed', 'recovered', 'deaths'],
  COV_GENERAL_INFO: [
    {
      Country: 'Afghanistan',
      CountryCode: 'AF',
      NewConfirmed: 243,
      TotalConfirmed: 50433,
      NewDeaths: 21,
      TotalDeaths: 2117,
      NewRecovered: 107,
      TotalRecovered: 39692,
    },
    {
      Country: 'Barbados',
      CountryCode: 'AF',
      NewConfirmed: 243,
      TotalConfirmed: 50433,
      NewDeaths: 21,
      TotalDeaths: 2117,
      NewRecovered: 107,
      TotalRecovered: 39692,
    },
    {
      Country: 'Lithuania',
      CountryCode: 'AF',
      NewConfirmed: 243,
      TotalConfirmed: 50433,
      NewDeaths: 21,
      TotalDeaths: 2117,
      NewRecovered: 107,
      TotalRecovered: 39692,
    },
  ],
};

export default TABLE_HEAD_LIST;
